package presentacion_web;

import entidades.Usuario;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "JueganaDB", urlPatterns = {"/JueganaDB"})
public class JueganaDB extends HttpServlet {
    
    
    //maquina gde: "root";
    //maquina net: "agdebian";
    public static final String NAME_DB = "jdbc:mysql://localhost/JUEGANA";
    public static final String USUARIO_DB = "";
    public static final String PASS_DB = "fortodb";
    
    
    //Driver MYSQL: JDBC
    public static final String DRIVER_MYSQL = "com.mysql.jdbc.Driver";
    
    
    //sentencias SQL
    public static final String NEW_USER = "INSERT INTO users (name_user, clave, mail) values( ?, ?, ?);";
    public static final String SQL_MODIF_USER = "UPDATE users clave=? mail=? WHERE id=? ;";
    
    /*Mensajes de tipo PrintWriter*/
    public static final String MENSAJE_ERROR = "Conectando ....Error";
    Gson convertir = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter output = response.getWriter();
            
        try {
            //0. Verificar Infraestructura, aquí El Driver
            Class.forName(DRIVER_MYSQL).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
          output.println(MENSAJE_ERROR + ex.getMessage() );
        }
        try {
            //1. Conectar con la base
            Connection conectar = DriverManager.getConnection(NAME_DB, USUARIO_DB, PASS_DB);
            output.println("Conectando....OK\n");
            //2. Crear la sentencia SQL
            PreparedStatement sentencia = conectar.prepareStatement("select * from users");
            output.println("Sentencia....OK\n");
            //3.Obtener el resultado
            ResultSet resultado = sentencia.executeQuery();
            output.println("Ejecuto la consulta en la base.... OK\n");
            //4. Mostrar el resultado
            while(resultado.next()){
                output.println(
                resultado.getString(1) + ", " +
                resultado.getString(2) + ", " +
                resultado.getString(3) + ", " +
                resultado.getString(4) + "\n"
                        );
            }
            output.println("Datos de la Base consultados ....OK");
        } catch (SQLException ex) {
            output.println("Conectando ....Error" + ex.getMessage() );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter output = response.getWriter();
        try {
            //0. Verificar Infraestructura, aquí El Driver
            Class.forName(DRIVER_MYSQL).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
          output.println(MENSAJE_ERROR + ex.getMessage() );
        }
        try {
            //1. Conectar con la base
            Connection conectar = DriverManager.getConnection(NAME_DB, USUARIO_DB, PASS_DB);
            output.println("Conectando....OK\n");
            //2. Crear la sentencia SQL
            PreparedStatement sentencia = conectar.prepareStatement(NEW_USER);
            output.println("Sentencia POST....OK\n");
            
            String texto = request.getReader().readLine();
            
            output.println("<p>el texto que me ley: siempre respetar nombres de atributos de la clase"
                    + "entre los dos lenguajes a comunicarse</p>" + texto);

            Usuario usuarioParam = convertir.fromJson(texto, Usuario.class);
                                    
            sentencia.setString(1, usuarioParam.getUserName());
            sentencia.setString(2, usuarioParam.getPassword());
            sentencia.setString(3, usuarioParam.getMail());
            
            
            //3.Obtener el resultado
            sentencia.execute();
            //4. Mostrar el resultado
            output.println("Datos de la Base Agregados! ....OK");
            output.println(convertir.toJson("OK ameo!"));
        } catch (SQLException ex) {
            output.println(MENSAJE_ERROR + ex.getMessage() );
        } catch (Exception ex){
            output.println(MENSAJE_ERROR + ex.getMessage() );
        }
        
        
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        
        try {
            
            PrintWriter out = response.getWriter();
            try {
                Class.forName(DRIVER_MYSQL).newInstance();
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                out.println("Verificar: " + ex.getMessage());
            }
            
            //Usuario usuarioParametro = convertir.fromJson(request.getReader(), Usuario.class);
            
            //Aqui formar Usuario DAO para descaoplar las ejecuciones de la setencia SQL.
            
            //1. conectar conla base.
            Connection c = DriverManager.getConnection(NAME_DB, USUARIO_DB, USUARIO_DB);
            
            //2a. preparar sentencia a ejecutar.
            PreparedStatement sentencia = c.prepareStatement(SQL_MODIF_USER);
            
            //3. Uso JSON para el intercambio de datos js-java.
            Usuario userParametro = convertir.fromJson( request.getReader(), Usuario.class);
            
            //2b. ingreso los datos obtenidos por JSON.
            sentencia.setString(0, userParametro.getId());
            sentencia.setString(2, userParametro.getPassword());
            sentencia.setString(3, userParametro.getMail());
            //2c. ejecuto la sentencia sql, con los datos obtenidos.
            sentencia.execute();
            
            //4. mostrar resultados
            out.println(convertir.toJson("OK, los datos fueron modificados!"));
            
            //5. cerrar el PreparedStatement Y la Conexion
            sentencia.close();
            c.close();
            
        } catch (SQLException ex) {
            System.err.println(" " + ex.getMessage());
        }
        
        
        
    }
    
    
    
    
}